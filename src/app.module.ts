import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './db.config';
import { EmployeeModule } from './employee/employee.module';
import { PayrollModule } from './payroll/payroll.module';
import { AppraisalModule } from './appraisal/appraisal.module';

@Module({
  imports: [TypeOrmModule.forRoot(typeOrmConfig), EmployeeModule, PayrollModule, AppraisalModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
