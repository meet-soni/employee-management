import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { PaginationDto } from 'src/helper/dto/pagination.dto';
import { ResponseDto } from 'src/helper/dto/response.dto';
import { ILike, Not, Repository } from 'typeorm';
import { CreatePayrollDto } from './dto/create-payroll.dto';
import { PayrollResponseDto } from './dto/payroll-response.dto';
import { UpdatePayrollDto } from './dto/update-payroll.dto';
import { Payroll } from './entities/payroll.entity';

@Injectable()
export class PayrollService {

  constructor(
    @InjectRepository(Payroll)
    private payrollRepository: Repository<Payroll>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>
  ) { }

  async create(createPayrollDto: CreatePayrollDto): Promise<PayrollResponseDto> {
    try {
      const createPayroll = new Payroll();
      createPayroll.creditedAmount = createPayrollDto.creditedAmount;
      createPayroll.creditedAt = createPayrollDto.creditedAt;
      createPayroll.employee = createPayrollDto.employee;

      const payroll = await this.payrollRepository.save(createPayroll);
      if (payroll && payroll.id) {
        return { statusCode: 200, message: 'Payroll has been created succesfully', data: payroll }
      }
      return { statusCode: 500, message: 'Internal Server Error' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }

  async findAll(options: PaginationDto): Promise<PayrollResponseDto> {
    try {
      const take = options.limit || 10;
      const skip = options.page || 1;
      const searchText = options.searchText || '';
      const sortField = options.sortField || 'createdAt';
      const sortOrder = options.sortOrder || 'DESC';

      const queryData = [];
      if (searchText) {
        queryData.push(
          { employee: ILike('%' + searchText + '%') },
          { creditedAmount: ILike('%' + searchText + '%') },
        );
      }

      const [payrolls, count] = await this.payrollRepository.findAndCount({
        where: queryData,
        order: {
          [sortField]: sortOrder.toUpperCase(),
        },
        take,
        skip: (skip - 1) * take
      });

      if (payrolls.length > 0) {
        return { statusCode: 200, message: 'Success', data: payrolls, count }
      }
      return { statusCode: 404, message: 'No records Found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) }
    }
  }

  async findOne(id: string): Promise<PayrollResponseDto> {
    try {
      const payroll = await this.payrollRepository.findOne({ id }, { relations: ['employee'] });
      if (payroll && payroll.id) {
        return { statusCode: 200, message: 'Success', data: payroll }
      }
      return { statusCode: 404, message: 'No records found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }

  async update(id: string, updatePayrollDto: UpdatePayrollDto): Promise<ResponseDto> {
    try {
      const employee = await this.payrollRepository.update({ id }, updatePayrollDto);
      if (employee.affected > 0) {
        return { statusCode: 200, message: 'Payroll Updated Successfully' }
      }
      return { statusCode: 404, message: 'Payroll not found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) }
    }
  }

  async remove(id: string): Promise<ResponseDto> {
    try {
      const result = await this.payrollRepository.delete(id);
      if (result.affected > 0) {
        return { statusCode: 200, message: 'Payroll deleted successfully' }
      }
      return { statusCode: 404, message: 'Record not found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }

  async getCreditedPayroll(): Promise<PayrollResponseDto> {
    try {

      const result = await this.payrollRepository.createQueryBuilder('pr')
        .select('pr.*')
        .addSelect('e.*')
        .leftJoin(Employee, 'e', `e.id = pr.employeeId`)
        .where(`MONTH(creditedAt) = MONTH(CURRENT_DATE())
      AND YEAR(creditedAt) = YEAR(CURRENT_DATE())`).getRawMany();
      if (result.length > 0) {
        return { statusCode: 200, message: 'Success', data: result }
      }
      return { statusCode: 404, message: 'Record not found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }

  async getPendingPayroll(): Promise<PayrollResponseDto> {
    try {
      const result = await this.payrollRepository.createQueryBuilder('pr')
        .select('e.id as id')
        .leftJoin(Employee, 'e', `e.id = pr.employeeId`)
        .where(`MONTH(creditedAt) = MONTH(CURRENT_DATE())
      AND YEAR(creditedAt) = YEAR(CURRENT_DATE())`).getRawMany();
      if (result.length > 0) {
        const ids = result.map(data => data.id);
        const employees = await this.employeeRepository.createQueryBuilder('e')
          .select('e.*')
          .where('e.id NOT IN (:...ids)', { ids: ids })
          .getRawMany();
        if (employees && employees.length > 0) {
          return { statusCode: 200, message: 'Success', data: employees }
        }
      }
      return { statusCode: 404, message: 'Record not found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }
}
