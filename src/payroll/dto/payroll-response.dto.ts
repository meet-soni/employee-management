import { ApiProperty } from "@nestjs/swagger";
import { ResponseDto } from "src/helper/dto/response.dto";
import { Payroll } from "../entities/payroll.entity";

export class PayrollResponseDto extends ResponseDto {

    @ApiProperty({ type: Payroll })
    data?: Payroll | Payroll[];

    @ApiProperty()
    count?: number;
}