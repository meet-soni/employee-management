import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsDateString, IsNotEmpty, IsObject } from "class-validator";
import { Employee } from "src/employee/entities/employee.entity";

export class CreatePayrollDto {

    @ApiProperty({ example: 1000, required: true })
    @IsNotEmpty()
    creditedAmount: number;

    @ApiProperty({ example: "2021-12-09", required: true })
    @IsDateString()
    creditedAt: string;

    @ApiProperty({ example:{"id": "73a7193b-543d-4a9d-9337-b344eea2203e"} ,required: true })
    @IsNotEmpty()
    @IsObject()
    employee: Employee;
}
