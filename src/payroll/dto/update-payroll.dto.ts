import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsNumber } from 'class-validator';

export class UpdatePayrollDto {

    @ApiProperty({ example: 1000, required: true })
    @IsNumber()
    creditedAmount: number;

    @ApiProperty({ example: "2021-12-09", required: true })
    @IsDateString()
    creditedAt: string;

 }
