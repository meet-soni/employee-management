import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { PayrollService } from './payroll.service';
import { CreatePayrollDto } from './dto/create-payroll.dto';
import { UpdatePayrollDto } from './dto/update-payroll.dto';
import { ApiTags } from '@nestjs/swagger';
import { PayrollResponseDto } from './dto/payroll-response.dto';
import { ResponseDto } from 'src/helper/dto/response.dto';
import { PaginationDto } from 'src/helper/dto/pagination.dto';

@ApiTags('Payroll')
@Controller('payroll')
export class PayrollController {
  constructor(private readonly payrollService: PayrollService) { }

  @Post()
  async create(@Body() createPayrollDto: CreatePayrollDto): Promise<PayrollResponseDto> {
    return await this.payrollService.create(createPayrollDto);
  }

  @Get('/credited')
  async getCreditedPayroll (): Promise<PayrollResponseDto>{
    return await this.payrollService.getCreditedPayroll();
  }

  @Get('/pending')
  async getPendingPayroll (): Promise<PayrollResponseDto>{
    return await this.payrollService.getPendingPayroll();
  }

  @Get()
  async findAll(@Query() params: PaginationDto): Promise<PayrollResponseDto> {
    return await this.payrollService.findAll(params);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<PayrollResponseDto> {
    return await this.payrollService.findOne(id);
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updatePayrollDto: UpdatePayrollDto): Promise<ResponseDto> {
    return this.payrollService.update(id, updatePayrollDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<ResponseDto> {
    return await this.payrollService.remove(id);
  }

}
