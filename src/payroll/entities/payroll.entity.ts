import { Employee } from "src/employee/entities/employee.entity";
import { Base } from "src/helper/base.entity";
import { Column, Entity, JoinTable, ManyToOne } from "typeorm";

@Entity()
export class Payroll extends Base {

    @Column({ type: 'bigint', nullable: false })
    creditedAmount: number

    @Column({ type: 'datetime', default: null })
    creditedAt: string;

    @ManyToOne(() => Employee, employee => employee.payrolls, { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: false })
    employee: Employee;
}
