import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import * as env from 'dotenv';
import { Appraisal } from "./appraisal/entities/appraisal.entity";
import { Employee } from "./employee/entities/employee.entity";
import { Payroll } from "./payroll/entities/payroll.entity";
env.config();

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'mysql',
    host: process.env.HOST || 'localhost',
    port: +process.env.DBPORT || 3306,
    username: process.env.USERNAME || 'root',
    password: process.env.PASSWORD || '',
    database: process.env.DATABASE || 'employee',
    entities: [Employee, Payroll, Appraisal],
    synchronize: true
}