import { ApiProperty } from "@nestjs/swagger";
import { ResponseDto } from "src/helper/dto/response.dto";
import { Appraisal } from "../entities/appraisal.entity";

export class AppraisalResponseDto extends ResponseDto {

    @ApiProperty({ type: Appraisal })
    data?: Appraisal | Appraisal[];

    @ApiProperty()
    count?: number;
}