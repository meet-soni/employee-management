import { ApiProperty } from "@nestjs/swagger";
import { IsDateString, IsNotEmpty, IsNumber, IsObject } from "class-validator";
import { Employee } from "src/employee/entities/employee.entity";

export class CreateAppraisalDto {

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    percentage: number;

    @ApiProperty({ example: '2021-06-20' })
    @IsDateString()
    @IsNotEmpty()
    appraisalDate: string;

    @ApiProperty({ example: { "id": "73a7193b-543d-4a9d-9337-b344eea2203e" }, required: true })
    @IsNotEmpty()
    @IsObject()
    employee: Employee;
}
