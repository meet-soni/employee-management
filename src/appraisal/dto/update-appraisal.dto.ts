import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsDateString, IsOptional } from 'class-validator';

export class UpdateAppraisalDto {

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    percentage: number;

    @ApiProperty({ example: '2021-09-20' })
    @IsDateString()
    @IsOptional()
    appraisalDate: string;
}
