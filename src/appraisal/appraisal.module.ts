import { Module } from '@nestjs/common';
import { AppraisalService } from './appraisal.service';
import { AppraisalController } from './appraisal.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Appraisal } from './entities/appraisal.entity';
import { Employee } from 'src/employee/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Appraisal, Employee])],
  controllers: [AppraisalController],
  providers: [AppraisalService]
})
export class AppraisalModule { }
