import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { AppraisalService } from './appraisal.service';
import { CreateAppraisalDto } from './dto/create-appraisal.dto';
import { UpdateAppraisalDto } from './dto/update-appraisal.dto';
import { AppraisalResponseDto } from './dto/appraisal-response.dto';
import { ApiTags } from '@nestjs/swagger';
import { ResponseDto } from 'src/helper/dto/response.dto';
import { PaginationDto } from 'src/helper/dto/pagination.dto';

@Controller('appraisal')
@ApiTags('Appraisal')
export class AppraisalController {
  constructor(private readonly appraisalService: AppraisalService) { }

  @Post()
  async create(@Body() createAppraisalDto: CreateAppraisalDto): Promise<AppraisalResponseDto> {
    return await this.appraisalService.create(createAppraisalDto);
  }

  @Get()
  async findAll(@Query() params: PaginationDto): Promise<AppraisalResponseDto> {
    return await this.appraisalService.findAll(params);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<AppraisalResponseDto> {
    return await this.appraisalService.findOne(id);
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateAppraisalDto: UpdateAppraisalDto): Promise<ResponseDto> {
    return await this.appraisalService.update(id, updateAppraisalDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<ResponseDto> {
    return await this.appraisalService.remove(id);
  }
}
