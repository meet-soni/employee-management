import { Employee } from "src/employee/entities/employee.entity";
import { Base } from "src/helper/base.entity"
import { Column, Entity, ManyToOne } from "typeorm"

@Entity()
export class Appraisal extends Base {

    @Column({ type: 'float' })
    percentage: number;

    @Column()
    appraisalAmount: number;

    @Column()
    currentSalary: number;

    @Column({ type: 'datetime' })
    appraisalDate: string;

    @ManyToOne(() => Employee, employee => employee.appraisals)
    employee: Employee;
}
