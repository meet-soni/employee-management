import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { PaginationDto } from 'src/helper/dto/pagination.dto';
import { ResponseDto } from 'src/helper/dto/response.dto';
import { ILike, Repository } from 'typeorm';
import { AppraisalResponseDto } from './dto/appraisal-response.dto';
import { CreateAppraisalDto } from './dto/create-appraisal.dto';
import { UpdateAppraisalDto } from './dto/update-appraisal.dto';
import { Appraisal } from './entities/appraisal.entity';

@Injectable()
export class AppraisalService {

  constructor(
    @InjectRepository(Appraisal)
    private appraisalRepository: Repository<Appraisal>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>
  ) { }


  async create(createAppraisalDto: CreateAppraisalDto): Promise<AppraisalResponseDto> {
    try {
      const employee = await this.employeeRepository.findOne({ id: createAppraisalDto.employee.id });
      if (employee && employee.id) {
        const newAppraisal = new Appraisal();
        newAppraisal.percentage = createAppraisalDto.percentage;
        newAppraisal.appraisalDate = createAppraisalDto.appraisalDate;
        newAppraisal.currentSalary = employee.salary;
        newAppraisal.employee = employee;
        newAppraisal.appraisalAmount = (employee.salary * (newAppraisal.percentage / 100));
        employee.salary = employee.salary + newAppraisal.appraisalAmount;

        const appraisal = await this.appraisalRepository.save(newAppraisal);
        if (appraisal && appraisal.id) {
          const updateSalary = await this.employeeRepository.save(employee);
          if (updateSalary && updateSalary.id) {
            return { statusCode: 200, message: 'Appraisal has been created successfully' }
          }
        }
        return { statusCode: 500, message: 'Something went wrong while creating appraisal' }
      }
      return { statusCode: 404, message: 'Employee not found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }

  async findAll(options: PaginationDto): Promise<AppraisalResponseDto> {
    try {
      const take = options.limit || 10;
      const skip = options.page || 1;
      const searchText = options.searchText || '';
      const sortField = options.sortField || 'createdAt';
      const sortOrder = options.sortOrder || 'DESC';

      const queryData = [];
      if (searchText) {
        queryData.push(
          { percentage: ILike('%' + searchText + '%') },
        );
      }

      const [appraisal, count] = await this.appraisalRepository.findAndCount({
        where: queryData,
        order: {
          [sortField]: sortOrder.toUpperCase(),
        },
        take,
        skip: (skip - 1) * take,
        relations: ['employee']
      });

      if (appraisal.length > 0) {
        return { statusCode: 200, message: 'Success', data: appraisal, count }
      }
      return { statusCode: 404, message: 'No records Found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) }
    }
  }

  async findOne(id: string): Promise<AppraisalResponseDto> {
    try {
      const appraisal = await this.appraisalRepository.findOne({ id }, { relations: ['employee'] });
      if (appraisal && appraisal.id) {
        return { statusCode: 200, message: 'Success', data: appraisal }
      }
      return { statusCode: 404, message: 'No record found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }

  async update(id: string, updateAppraisalDto: UpdateAppraisalDto): Promise<ResponseDto> {
    try {
      const appraisal = await this.appraisalRepository.findOne(id, { relations: ['employee'] });
      if (appraisal && appraisal.id) {
        const newAppraisal = appraisal;
        newAppraisal.id = appraisal.id;
        newAppraisal.percentage = (updateAppraisalDto.percentage !== undefined) ? updateAppraisalDto.percentage : newAppraisal.percentage;
        newAppraisal.appraisalDate = (updateAppraisalDto.appraisalDate) ? updateAppraisalDto.appraisalDate : newAppraisal.appraisalDate;
        newAppraisal.appraisalAmount = (updateAppraisalDto.percentage !== undefined) ? (newAppraisal.currentSalary * (newAppraisal.percentage / 100)) : newAppraisal.appraisalAmount;
        newAppraisal.employee.salary = (updateAppraisalDto.percentage !== undefined) ? (newAppraisal.currentSalary + newAppraisal.appraisalAmount) : newAppraisal.employee.salary;

        const updateAppraisal = await this.appraisalRepository.save(newAppraisal);
        if (updateAppraisal && updateAppraisal.id) {
          const updateSalary = await this.employeeRepository.save(newAppraisal.employee);
          if (updateSalary && updateSalary.id) {
            return { statusCode: 200, message: 'Appraisal updated successfully' }
          }
        }
        return { statusCode: 404, message: 'Appraisal not found' }
      }
      return { statusCode: 404, message: 'Record not found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }

  async remove(id: string): Promise<ResponseDto> {
    try {
      const appraisal = await this.appraisalRepository.findOne(id, { relations: ['employee'] })
      if (appraisal && appraisal.id) {
        appraisal.employee.salary = appraisal.currentSalary;
        const updateSalary = await this.employeeRepository.save(appraisal.employee);
        const result = await this.appraisalRepository.delete(id);
        if (result && result.affected > 0) {
          return { statusCode: 200, message: 'Appraisal deleted successfully' }
        }
      }
      return { statusCode: 404, message: 'Record not found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) };
    }
  }
}
