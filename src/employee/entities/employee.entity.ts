import { Exclude } from "class-transformer";
import { Appraisal } from "src/appraisal/entities/appraisal.entity";
import { Base } from "src/helper/base.entity";
import { Payroll } from "src/payroll/entities/payroll.entity";
import { Column, Entity, OneToMany } from "typeorm";

@Entity()
export class Employee extends Base {

    @Column({ type: 'varchar' })
    firstName: string;

    @Column({ type: 'varchar' })
    lastName: string;

    @Column({ type: 'varchar', nullable: false, unique: true })
    email: string;

    @Column({ nullable: false })
    mobile: string;

    @Column({ nullable: false })
    @Exclude()
    password: string;

    @Column({ nullable: true })
    address: string;

    @Column({ nullable: true, default: null })
    city: string;

    @Column({ nullable: true, default: null })
    state: string;

    @Column({ nullable: true, default: null })
    country: string;

    @Column({ nullable: false })
    userRole: string;

    @Column({ nullable: false })
    designation: string;

    @Column({ nullable: false, default: 0 })
    salary: number;

    @OneToMany(() => Payroll, payroll => payroll.employee)
    payrolls: Payroll[];

    @OneToMany(() => Appraisal, appraisal => appraisal.employee)
    appraisals: Appraisal[];
}
