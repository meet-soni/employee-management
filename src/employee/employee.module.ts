import { Module } from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { EmployeeController } from './employee.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from './entities/employee.entity';
import { DuplicateEmailValidator } from 'src/helper/email-validator';

@Module({
  imports:[TypeOrmModule.forFeature([Employee])],
  controllers: [EmployeeController],
  providers: [EmployeeService, DuplicateEmailValidator],
  exports: [EmployeeService]
})
export class EmployeeModule {}
