import { ApiProperty } from "@nestjs/swagger";
import { ResponseDto } from "src/helper/dto/response.dto";
import { Employee } from "../entities/employee.entity";
import { CreateEmployeeDto } from "./create-employee.dto";

export class createEmployeeResponseDto extends ResponseDto {

    @ApiProperty({ type: Employee })
    data?: Employee | Employee[];

    @ApiProperty()
    count?: number;
}