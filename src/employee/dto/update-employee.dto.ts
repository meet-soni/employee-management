import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsOptional, IsString, IsNumber, IsEnum } from "class-validator";
import { Roles } from "src/helper/enum/role.enum";


export class UpdateEmployeeDto {

    @ApiProperty({ example: 'admin@gmail.com' })
    @IsEmail({}, { message: 'Please enter a valid email' })
    email: string;

    @ApiProperty({ example: 'James' })
    @IsString()
    firstName: string;

    @ApiProperty({ example: 'Bond' })
    @IsString()
    lastName: string;

    @ApiProperty({ example: '9876543210' })
    @IsString()
    mobile: string;

    @ApiProperty({ example: 'A0 Test building' })
    @IsOptional()
    @IsString()
    address: string;

    @ApiProperty({ example: 'Ahmedabad' })
    @IsOptional()
    @IsString()
    city: string;

    @ApiProperty({ example: 'gujarat' })
    @IsOptional()
    @IsString()
    state: string;

    @ApiProperty({ example: 'india' })
    @IsOptional()
    @IsString()
    country: string;

    @ApiProperty({ example: 'EMPLOYEE', required: true, default: Roles.EMPLOYEE })
    @IsEnum(Roles)
    userRole: Roles;

    @ApiProperty({ example: 'Trainee' })
    @IsString()
    designation: string;

    @ApiProperty({ example: 8000 })
    @IsNumber()
    salary: number;
}
