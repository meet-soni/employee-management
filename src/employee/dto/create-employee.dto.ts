import { ApiProperty } from "@nestjs/swagger";
import { Exclude } from "class-transformer";
import { IsEmail, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString, MinLength, Validate } from "class-validator";
import { DuplicateEmailValidator } from "src/helper/email-validator";
import { Roles } from "src/helper/enum/role.enum";
export class CreateEmployeeDto {

    @ApiProperty({ example: 'admin@gmail.com', required: true })
    @IsEmail({}, { message: 'Please enter a valid email' })
    @Validate(DuplicateEmailValidator)
    email: string;

    @ApiProperty({ example: 'password', required: false })
    @MinLength(1)
    @IsNotEmpty()
    password: string;

    @ApiProperty({ example: 'James', required: true })
    @IsNotEmpty()
    firstName: string;

    @ApiProperty({ example: 'Bond', required: true })
    @IsNotEmpty()
    lastName: string;

    @ApiProperty({ example: '9876543210', required: true })
    @IsNotEmpty()
    // @MinLength(10, { message: 'Please Enter valid Mobile Number' })
    mobile: string;

    @ApiProperty({ example: 'A0 Test building' })
    @IsOptional()
    address: string;

    @ApiProperty({ example: 'Ahmedabad' })
    @IsOptional()
    city: string;

    @ApiProperty({ example: 'gujarat' })
    @IsOptional()
    state: string;

    @ApiProperty({ example: 'india' })
    @IsOptional()
    country: string;

    @ApiProperty({ example: 'EMPLOYEE', required: true, default: Roles.EMPLOYEE })
    @IsEnum(Roles)
    @IsNotEmpty()
    userRole: Roles;

    @ApiProperty({ example: 'Trainee', required: true })
    @IsNotEmpty()
    designation: string;

    @ApiProperty({ example: 8000, required: true })
    @IsNotEmpty()
    @IsNumber()
    salary: number;
}
