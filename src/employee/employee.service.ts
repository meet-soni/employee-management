import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationDto } from 'src/helper/dto/pagination.dto';
import { ResponseDto } from 'src/helper/dto/response.dto';
import { ILike, Not, Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { createEmployeeResponseDto } from './dto/employee-response.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeeService {

  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) { }

  async getEmployeeByEmail(email: string) {
    return await this.employeeRepository.findOne({ email })
  }

  async create(createEmployeeDto: CreateEmployeeDto): Promise<createEmployeeResponseDto> {
    try {
      const data = { ...createEmployeeDto }
      const createEmployee = await this.employeeRepository.save(data);
      if (createEmployee && createEmployee.id) {
        return { statusCode: 200, message: 'Employee has been created succesfully', data: createEmployee }
      } else {
        return { statusCode: 500, message: 'Internal Server Error' }
      }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) }
    }
  }

  async findAll(options: PaginationDto): Promise<createEmployeeResponseDto> {
    try {
      const take = options.limit || 10;
      const skip = options.page || 1;
      const searchText = options.searchText || '';
      const sortField = options.sortField || 'createdAt';
      const sortOrder = options.sortOrder || 'DESC';

      const queryData = [];
      if (searchText) {
        queryData.push(
          { email: ILike('%' + searchText + '%'), userRole: Not('ADMIN') },
          { firstName: ILike('%' + searchText + '%'), userRole: Not('ADMIN') },
          { lastName: ILike('%' + searchText + '%'), userRole: Not('ADMIN') },
          { mobile: ILike('%' + searchText + '%'), userRole: Not('ADMIN') },
          { salary: ILike('%' + searchText + '%'), userRole: Not('ADMIN') },
          { designation: ILike('%' + searchText + '%'), userRole: Not('ADMIN') },
        );
      } else {
        queryData.push({ userRole: Not('ADMIN') });
      }

      const [employees, count] = await this.employeeRepository.findAndCount({
        where: queryData,
        order: {
          [sortField]: sortOrder.toUpperCase(),
        },
        take,
        skip: (skip - 1) * take
      });

      if (employees.length > 0) {
        return { statusCode: 200, message: 'Success', data: employees, count }
      }
      return { statusCode: 404, message: 'No records Found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) }
    }
  }

  async findOne(id: string): Promise<createEmployeeResponseDto> {
    try {
      const employee = await this.employeeRepository.findOne({ id: id });
      if (employee && employee.id) {
        return { statusCode: 200, message: 'success', data: employee }
      }
      return { statusCode: 404, message: 'No records Found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) }
    }
  }

  async update(id: string, updateEmployeeDto: UpdateEmployeeDto): Promise<ResponseDto> {
    try {
      const employee = await this.employeeRepository.update({ id: id }, updateEmployeeDto);
      if (employee.affected > 0) {
        return { statusCode: 200, message: 'Employee Updated Successfully' }
      }
      return { statusCode: 404, message: 'Employee not found' }
    } catch (error) {
      console.log("🚀 ~ file: employee.service.ts ~ line 97 ~ EmployeeService ~ update ~ error", error)
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) }
    }
  }

  async remove(id: string): Promise<ResponseDto> {
    try {
      const result = await this.employeeRepository.delete({ id: id });
      if (result && result.affected === 1) {
        return { statusCode: 200, message: 'Employee has been deleted successfully' }
      }
      return { statusCode: 404, message: 'No records Found' }
    } catch (error) {
      return { statusCode: 500, message: 'Internal Server Error', errorData: JSON.stringify(error) }
    }
  }

}
