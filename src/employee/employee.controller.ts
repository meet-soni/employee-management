import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { createEmployeeResponseDto } from './dto/employee-response.dto';
import { plainToClass } from 'class-transformer';
import { ApiTags } from '@nestjs/swagger';
import { ResponseDto } from 'src/helper/dto/response.dto';
import { PaginationDto } from 'src/helper/dto/pagination.dto';

@ApiTags('Employee')
@Controller('employee')
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService) { }

  @Post()
  async create(@Body() createEmployeeDto: CreateEmployeeDto): Promise<createEmployeeResponseDto> {
    const result = await this.employeeService.create(createEmployeeDto);
    return plainToClass(createEmployeeResponseDto, result, { strategy: 'exposeAll' })
  }

  @Get()
  async findAll(@Query() params: PaginationDto): Promise<createEmployeeResponseDto> {
    const result = await this.employeeService.findAll(params);
    return plainToClass(createEmployeeResponseDto, result);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<createEmployeeResponseDto> {
    const result = await this.employeeService.findOne(id);
    return plainToClass(createEmployeeResponseDto, result);
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateEmployeeDto: UpdateEmployeeDto): Promise<ResponseDto> {
    return await this.employeeService.update(id, updateEmployeeDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<ResponseDto> {
    return await this.employeeService.remove(id);
  }
}
