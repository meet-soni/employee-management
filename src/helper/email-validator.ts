import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { EmployeeService } from '../employee/employee.service';


@ValidatorConstraint({ name: 'email', async: true })
export class DuplicateEmailValidator implements ValidatorConstraintInterface {
    constructor(
        readonly employeeService: EmployeeService
    ) { }

    async validate(email: string) {
        const user = await this.employeeService.getEmployeeByEmail(email);
        return !user;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    defaultMessage(args: ValidationArguments) {
        return 'Email is already in use';
    }
}
