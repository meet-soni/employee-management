import { IsString, IsNumberString, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PaginationDto {

    @ApiProperty({ required: false })
    @IsOptional()
    @IsNumberString()
    page: number;

    @ApiProperty({ required: false })
    @IsOptional()
    @IsNumberString()
    limit: number;

    @ApiProperty({ required: false })
    @IsOptional()
    @IsString()
    searchText: string;

    @ApiProperty({ required: false })
    @IsOptional()
    @IsString()
    sortOrder: string;

    @ApiProperty({ required: false })
    @IsOptional()
    @IsString()
    sortField: string;
}